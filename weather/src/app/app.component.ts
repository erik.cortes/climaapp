import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule } from '@angular/forms';
import { WeatherService } from './service/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[WeatherService]
})
export class AppComponent implements OnInit{
  weather;
  public unit:any;


  constructor(private weatherservice: WeatherService){//class WeatherService from weather.service.ts
   
    this.unit = {
      metric: ''
    }
    
  }

  ngOnInit(){
   
    }

    getWeather(lat,lon,units){
       //getCoords void from weather.service.ts
     this.weatherservice.getCoords(lat, lon,units)
     
     //while we return getCoords data with this, we can use subscribe
      .subscribe(
        res => {
          console.log(res);
          this.weather = res
        },
        err => console.log(err)

        /*Returning data by console */
       /*  res => console.log(res),
        err => console.log(err) */
      )

    }
    submitLocation(lat,lon,units){
      if(lat.value && lon.value){
      this.getWeather(lat.value,lon.value,units.value);
     /*  console.log(lat.value,lon.value);//print by console */
      lat.value = '';
      lon.value = '';
    }else {
      alert('error en los campos, inserte valores');

    }
      lat.focus();
      return false;



    }
  /*   getWeather(lat:number,lon:number){
      this.weatherservice.getCoords(34,123)

      .subscribe(
        res => console.log(res),
        err => console.log(err)
      )


    }
    submitLocation(lon,lat){
      this.getWeather(lon.value,lat.value);
      lon.value = '';
      lat.value = '';
      lon.focus();

      return false;

    } */
  }
  

