import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AppComponent} from 'src/app/app.component';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  //apikey and url stored in variables
  
  url = 'https://api.openweathermap.org/data/2.5/weather?units=metric';
  apikey = "685fb44315c4a831e484efbb03ce5195";
  lang = 'es';
  units ='';
  //url = "api.openweathermap.org/data/2.5/weather";
  //api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}

  constructor(private http:HttpClient) { //we need httpClient in order to obtain a response from api 
   

  }

  getCoords(lat,lon,units){
    //set variables by httpparams
    let params = new HttpParams()
    .set('lat',lat)
    .set('lon',lon)
    .set('lang',this.lang)
    .set('units',units)
    .set('appid',this.apikey)
    
    return this.http.get(this.url, {params}); //concat url and params in order to get weather by coordinates

  }
  

  
}
